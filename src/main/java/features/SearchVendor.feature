Feature: Login and Search the Vendor using VendorTaxID

@smoke
Scenario Outline: Positive Flow
Given Open the Chrome Browser
And Maximize the Window
And Set the timeouts
And Load the URL
And Enter the email as <uname>
And Enter the password as <password>
And Click on the Login Button
And Mouseover on the Vendors Tab
And Click on the Search for Vendors
And Enter the Vendor <taxid>
When Click on the Search Button
Then Print the vendor name

Examples:
|uname|password|taxid|
|mrskumar86@gmail.com|dhatcha1|IT231232|
|mrskumar86@gmail.com|dhatcha1|RO094782|
|mrskumar86@gmail.com|dhatcha1|FR329083|