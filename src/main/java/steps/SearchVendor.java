package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SearchVendor {
	
	ChromeDriver driver;
	Actions builder;
	
	@Given("Open the Chrome Browser")
	public void openTheChromeBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	    //throw new cucumber.api.PendingException();
	}

	@Given("Maximize the Window")
	public void maximizeTheWindow() {
		driver.manage().window().maximize();
	    //throw new cucumber.api.PendingException();
	}

	@Given("Set the timeouts")
	public void setTheTimeouts() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    //throw new cucumber.api.PendingException();
	}

	@Given("Load the URL")
	public void loadTheURL() {
		driver.get("https://acme-test.uipath.com/account/login");
	    //throw new cucumber.api.PendingException();
	}

	@Given("Enter the email as (.*)")
	public void enterTheEmailID(String data) {
		driver.findElementById("email").clear();
		driver.findElementById("email").sendKeys(data);
	    //throw new cucumber.api.PendingException();
	}

	@Given("Enter the password as (.*)")
	public void enterThePassword(String data) {
		driver.findElementById("password").clear();
		driver.findElementById("password").sendKeys(data);
	    //throw new cucumber.api.PendingException();
	}

	@Given("Click on the Login Button")
	public void clickOnTheLoginButton() {
		driver.findElementById("buttonLogin").click();
	    //throw new cucumber.api.PendingException();
	}

	@Given("Mouseover on the Vendors Tab")
	public void mouseoverOnTheVendorsTab() {
		builder = new Actions(driver);
		builder.moveToElement(driver.findElementByXPath("//button[text()=' Vendors']")).perform();
	    //throw new cucumber.api.PendingException();
	}

	@Given("Click on the Search for Vendors")
	public void clickOnTheSearchForVendors() {
		builder = new Actions(driver);
		builder.moveToElement(driver.findElementByLinkText("Search for Vendor")).click().perform();
	    //throw new cucumber.api.PendingException();
	}

	@Given("Enter the Vendor (.*)")
	public void enterTheVendorTaxID(String data) {
		driver.findElementById("vendorTaxID").clear();
		driver.findElementById("vendorTaxID").sendKeys(data);
	    //throw new cucumber.api.PendingException();
	}

	@When("Click on the Search Button")
	public void clickOnTheSearchButton() {
		driver.findElementById("buttonSearch").click();
	    //throw new cucumber.api.PendingException();
	}

	@Then("Print the vendor name")
	public void printTheVendorName() {
		String text = driver.findElementByXPath("//tr[2]/td[1]").getText();
		System.out.println(text);
	    //throw new cucumber.api.PendingException();
	}
}
